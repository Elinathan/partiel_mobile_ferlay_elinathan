import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import Select from 'react-select'
import { FlatList, StyleSheet, Text, View, TextInput, Button, Pressable } from 'react-native';

const DATAS = [
  {name: "Medal of Honor", price: "10€", catégorie : "FPS", id: 23124},
  {name: "Street Fighter 2", price: "20€", catégorie : "Combat", id: 12349},
  {name: "Call of Duty", price: "30€", catégorie : "FPS", id: 549762},
  {name: "NBA2K5", price: "5€", catégorie : "Sport", id: 549763},
  {name: "God Of War 2018", price: "25€", catégorie : "Action-Aventure", id: 549764},
  {name: "The Legend of Zelda : The Wind Walker", price: "35€", catégorie : "Action-Aventure", id: 549765},
  {name: "Horizon : Forbidden West", price: "40€", catégorie : "Action-Aventure", id: 549766},
  {name: "Forza Horizon 5", price: "45€", catégorie : "Voiture", id: 549767},
  {name: "The Last Of Us", price: "55€", catégorie : "Survival horror", id: 549768},
  {name: "Red Dead Redemption II", price: "18€", catégorie : "Action-Aventure", id: 549769}] 


export default function App() {

  const [videoGameList, setVideoGameList] = useState(DATAS);

  let pseudo = "Votre pseudo";
  const [nbVideoGame, setNbVideoGame] = useState(videoGameList.length);  


  const VideoGameView = ({nom, prix, categorie}) => {
    return (
      <View style={styles.videoGame}>
        <Text>{nom} | {prix}</Text>
        <Text>#{categorie}</Text>
      </View>
    );
  };  

  const renderVideoGameView = ({item}) => {
    return (
      <VideoGameView
        nom= {item.name}
        prix={item.price}
        categorie={item.catégorie}
      />
    );
  };


  const CategorieView = ({categorie}) => {
    return (
      <View>
        <Pressable onPress={() => {
          setVideoGameList(DATAS.filter(jeu => jeu.catégorie == categorie))
        }}>
          <Text>{categorie}</Text>
        </Pressable>
      </View>
    );
  };  

  const renderCategorieView = ({item}) => {
    return (
      <CategorieView
        categorie={item.catégorie}
      />
    );
  };

  let nom = "";
  let prix = "";
  let catégorie = "";

  const idAleatoire = () => {
    let number = Math.floor(Math.random()*1000000);
    return number;
  };  

  return (
    <View style={styles.container}>

      <View style = {styles.header}>
        <Text> {pseudo}                                  Nb jeux video: {nbVideoGame}</Text>
      </View>

      <View>
        <Text>Filtrer par catégorie: </Text>
        <View style={styles.list2}>
          <FlatList
            data={DATAS}
            renderItem={renderCategorieView}
          />
        </View>
        <View>
            <Pressable onPress={() => {
              setVideoGameList(DATAS)
            }}>
              <Text style={styles.souligner}>Réinitialiser</Text>
            </Pressable>
          </View>
      </View>

      <View style={styles.list}>
        <FlatList
          data={videoGameList}
          renderItem={renderVideoGameView}
        />
      </View>

      <View style={styles.addGame}>
        <Text>Ajouter un jeu vidéo</Text>
        <View>
          <Text>Nom : </Text>
          <TextInput
            style={styles.input}
            onChangeText={newGameNom => nom = newGameNom}
          />
          <Text>Prix : </Text>
          <TextInput
            style={styles.input}
            keyboardType="numeric"
            onChangeText={newGamePrix => prix = newGamePrix+"€"}
          />
          <Text>Catégorie : </Text>
          <TextInput
            style={styles.input}
            onChangeText={newGameCategorie => catégorie = newGameCategorie}
          />
          <View style={styles.button}>
            <Button
              title='+'
              onPress={() => {
                let game = {name: nom, price: prix, catégorie: catégorie, id: idAleatoire()}
                videoGameList.push(game)
                setNbVideoGame(videoGameList.length)}}
            />
          </View>
        </View>

      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    backgroundColor: "lightblue",
    width: 320,
    height: 40,
  },
  list: {
    marginTop: 30,
    marginBottom: 30,
    width: 250,
    height: 300,
    borderColor: 'black',
    borderWidth: 3,
  },
  list2: {
    width: 100,
    height: 30,
    borderColor: 'black',
    borderWidth: 3,
  },
  input: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
  },
  button: {
    width: 40,
    height: 40,
    margin: 5,
  },
  addGame: {
    alignItems: 'center',
    backgroundColor: 'lightblue',
    width: 225,
    height: 225,
  },
  videoGame: {
    alignItems: 'center',
    margin: 10,
  }, souligner: {
    textDecorationLine: 'underline'
  }
});
